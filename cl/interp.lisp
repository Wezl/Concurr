(load #p"read.lisp")
(defpackage :concurr-interpreter
  (:use :common-lisp)
  (:import-from :concurr-read :read-expr :module :block :list :quote)
  (:export :repl :interp))
(in-package :concurr-interpreter)

(defun repl()
  (format t "> ")
  (loop while (peek-char nil *standard-input* nil)
        do (format t "~s~%> " (interp (read-expr) (make-env) *cnc-user*))))

;env: ((var . value) (v . v) (v . v) ...)
(defun interp (value &optional (env (make-env)) (module (make-module)))
  (cond
    ((consp value)
      (case (car value)
        (module (from-module (cdr value) module))
        (quote (cdr value))
        (block (cons (cons module env) (cdr value)))
        (list (mapcar #'(lambda (v) (interp v env module))
                      (cdr value)))
        (otherwise
          (let ((fn (interp (car value) env module))
                (arg (interp (cdr value) env module)))
            (if (functionp fn)
              (funcall fn arg env module)
              (funcall
                (funcall (from-module 'cnc-sym::|apply| module) fn)
                arg))))))
    ((symbolp value) (cdr (or (assoc value env)
                              (error (format nil "no variable '~a'" value)))))
    ((stringp value) value)
    ((numberp value) value)
    (t (error (format nil "'~s' is not valid concurr!" value)))))

(defconstant *prelude* (make-hash-table))

(defun make-module(&optional (parent *prelude*))
  (let ((it (make-hash-table)))
    (setf (gethash 'cnc-sym::|parent| it) parent)
    it))

(defun from-module (key module)
  (multiple-value-bind (val found) (gethash key module)
    (if found
        val
        (from-module key
          (or (gethash 'cnc-sym::|parent| module)
              (error (format nil "Can't find ~a in module" key)))))))

(defun make-env ()
  '((module . module) (quote . quote) (block . block) (list . list)))
  ;; this is quoted, to remind that you shouldn't modify environments ;)

(defconstant *cnc-user* (make-module))

(defmacro cncfn (name args &body body)
  (loop for arg in (reverse args)
        do (setf body `((lambda (,arg env module) . ,body))))
  `(setf (gethash ',(intern name :cnc-sym) *prelude*)
     . ,body))

(cncfn "+" (a b) (+ a b))
(cncfn "-" (a b) (- a b))
(cncfn "let" (name val body)
  (interp (cdr body) (cons (cons name val) (cdar body)) (caar body)))
(cncfn "lambda" (name body val)
  (interp (cdr body) (cons (cons name val) (cdar body)) (caar body)))
(cncfn "do" (body)
  (if (consp body)
    (interp (cdr body) (cdar body) (caar body))
    body))
(cncfn "true"  (choicea choiceb)
  (interp (cdr choicea) (cdar choicea) (caar choicea)))
(cncfn "false" (choicea choiceb)
  (interp (cdr choiceb) (cdar choiceb) (caar choiceb)))
(cncfn "=" (a b)
  (from-module (if (eql a b) 'cnc-sym::|true| 'cnc-sym::|false|) *prelude*))
(cncfn "global" (name body)
  (setf (gethash name module) body))
(cncfn "cons" (a b) (cons a b))
(cncfn "l" (cons) (car cons))
(cncfn "r" (cons) (cdr cons))
(cncfn "cons?" (v) (consp v))
(cncfn "apply" (v)
  (cond
    ((functionp v) v)))


