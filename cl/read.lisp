(defpackage :concurr-symbols
  (:use) ; don't use common-lisp symbols
  (:nicknames :cnc-sym)
  (:documentation "stores symbols read from ConCurr source"))
(defpackage :concurr-read
  (:documentation "read ConCurr source")
  (:export :read-expr :module :block :list :quote)
  (:use common-lisp))
(in-package :concurr-read)

(defconstant *whitespace* (list #\space #\newline #\tab #\return))
(defconstant *metachars*
  (list* #\# #\: #\{ #\} #\( #\) #\[ #\] #\" #\$ #\| #\' nil *whitespace*))
; nil meaning end of file
(defconstant *closing* (list #\) #\} #\] #\) nil))


(defun consume (ch s)
  (if (char= ch (peek-char nil s nil)) (read-char s) nil))

(defun expect (ch s)
  (or (consume ch s)
      (error (format nil "expected '~a' but got '~a'!" ch
                     (or (read-char s nil) "EOF")))))

(defun read-expr (&optional (s *standard-input*))
  (skipws s)
  (case (peek-char nil s)
    ((#\( #\[ #\: #\$) (read-struct s))
    (#\{ (read-list s))
    (#\" (read-string s))
    (#\\ (read-char s) (cons 'quote (read-expr s)))
    (#\| (read-pipe-atom s))
    (#\# (read-char s)
         (case (peek-char nil s)
           ; put pragmas here
           (otherwise (skip-comment s) (read-expr s))))
    (otherwise (read-atom s))))

(defun skipws (s)
  (loop while (member (peek-char nil s) *whitespace*)
        do (read-char s)))

(defun skip-comment (s)
  (cond
     ((consume #\! s)
      (loop until (and (consume #\! s) (consume #\# s))
            if (consume #\# s) do (skip-comment s)
            else do (read-char s)))
     ((consume #\; s)
      (read-expr s))
     (t (read-line))))

(defun escape (c)
  (case c
    (#\n #\newline)
    (#\t #\tab)
    (#\r #\return)
    (otherwise c)))

(defun read-pipe-atom (s)
  (expect #\| s)
  (make-symbol (coerce
     (loop for c = (or (read-char s nil) (error "unclosed |symbol|"))
           until (char= c #\|)
           if (char= c #\\) collect (escape (read-char s))
              else collecting c)
     'string)))
   ;"|name|" is the same as "name", but it allows putting any
   ; character in the name, like "|asdf;kls\`jfa#12f:()/|"

(defun read-atom (s)
  (let ((v (coerce
             (loop until (member (peek-char nil s nil) *metachars*)
                   collect (read-char s))
           'string)))
    (or (parse-number v)
        (intern v :concurr-symbols))))

(defun parse-number (v)
  (multiple-value-bind (integer end) (parse-integer v :junk-allowed t)
    (cond
      ((null integer) nil)
      ((eql end (length v)) integer)
      ((eql (elt v end) #\.)
        (incf end)
        (let ((leading-zeroes 0))
          (loop while (eql (elt v end) #\0)
                do (incf end) do (incf leading-zeroes))
          (multiple-value-bind (decimal end)
              (parse-integer v :start end :junk-allowed t)
            (and decimal
                 (eql end (length v))
                 (loop until (< decimal 1)
                       do (setf decimal (/ decimal 10))
                       finally (return t))
                 (* (+ (abs integer) (/ decimal (expt 10 leading-zeroes)))
                    (if (< integer 0) -1 1)))))))))

(defun read-string (s)
  (expect #\" s)
  (coerce
     (loop for c = (or (read-char s) (error "unclosed \"string\""))
           until (char= c #\")
           if (char= c #\\) collect (escape (read-char s))
              else collecting c)
     'string))

(defun read-list (s)
  (expect #\{ s)
  (skipws s)
  (let ((list (loop until (member (peek-char nil s) *closing*)
                    collect (read-expr s)
                    do (skipws s))))
     (expect #\} s)
     (when (consume #\: s)
       (setf (cdr (last list)) (read-expr s)))
     (cons 'list list)))

(defun read-struct (s)
  (let ((type   (read-char s))
        (struct (if (consume #\: s) (read-expr s) 'module)))
    (skipws s)
    (loop until (member (peek-char nil s) *closing*)
          ;do (format t "found ~c" (peek-char nil s))
          do (setf struct (cons struct (read-expr s)))
          do (skipws s))
    (case type
      (#\[ (expect #\] s)
           (cons 'block struct))
      (#\( (expect #\) s) struct)
      (#\$ struct)
      (#\: (cons 'block struct)))))

