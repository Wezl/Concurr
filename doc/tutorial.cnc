#!/usr/bin/env sh
./cnc --option $0; exit

License: #!

Copyright (C) 2020 by Cedrice Ermineii <CedriceRmineii@protonmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.

!# /license

# ConCurr

## Comments

 - the hash or number sign starts comments
 - hash-bang starts and bang-hash ends nestable
   multi-line commments

## Define

 - an example:
!#

#!# (do2 [first] [second])
 ! evaluate $first then $second
 ! for imperative code
 !#

(define [do2 fst snd]
  : let \_ $ value fst
      : value second)
#!
 - The brackets show that it's not immediately
   evaluated. It's like quoting in lisp, except
   there are no "special forms". If you're not
   evaluating it, quote it. There are no macros
   either because macros don't make sense as
   first-class data. If you want one, use a fun-
   ction instead.

## Variables

!#
(define [PI] 3.14159265359) # global, immutable

(let \one 1
  : define TWO $ + one one) # equivalent to:
# : define TWO ( + one one))

(let \mutvar 0
  : define [next n]
      : set! \mutvar (+ mutvar n) \n)
#!
 - let to lexically bind a value in an expression
 - define to introduce a constant
 - set! to set let-bound variable, but you shouldn't

## Pattern matching

 - Church booleans
!#

#!# (if cond [iftrue] [iffalse])
 ! for people who need keywords
 !#
(define [if cond yes no]
  : cond [value yes] # calling cond as a function
   : value no) # else

#!# (odd? num), (even? num)
 ! recursive functions to tell whether it's odd or even
 ! (That's pretty self-explanatory, so this comment isn't
 ! really necessary. Choose your own style)
 !#
(define [odd? a]
  : = a 0 [false]
  : < a 0 [odd? $ neg a]
   : even? $ - a 1)
(define [even? a]
   : not $ odd? a)

#!# (map function vallist)
 ! another slow recursive function, this one processing
 ! a list. Returns a list where each element is the co-
 ! rresponding element in the original list, processed
 ! by function.
 !#
(define [map fn lst]
  : null? lst []
  : consof lst \v \vs
      [ cons (fn v) $ map fn vs]
  : error "type-error")
#!
 - predicates conventionally end in ?
 - patterns that bind variables should end in "of"
 - there should always be a default case

## Anonymous and Point-free

 - !#(lambda \var [body])#! works to create functions,
   but it's not always the most convenient way.
!#
(define inc $ + 1 )

(define [swap fn a b]: fn b a)
(define halve $ swap (/) 2)

(define [|> fna fnb arg]: fnb $ fna arg)
(define composition-example
   $|> (+ 1) $|> (* 2) $|> (/ 3) (+ 4) )
# same as (\x -> (x+1)*2 / 3  +  4)

(define type $
  case {
    (cons?) \"cons"
    (null?) \"nil"
    (str?) \"str"
    (fun?) \"fun!"
    (num?) \"num"
     : format "unknown: %q" # does not output
  })
#!
 - currying means that a function can be partially
   applied conveniently
 - higher order functions and composition can also be
   convenient
 - !#(case {(?)[] (?)[] ... [default]})#! returns a
   function that will match against a value.

## Datatypes

 - dynamic typing
 - type predicates are cons? list? num? str? fun? null?
 - = to compare
 - string to turn anything into a string
 - read to read anything and readnum to read a number
   from a string

* Numbers

 - these act like doubles but may be stored as integers
 - / + * and - are arithmetic *functions* that take 2
   arguments. mod, neg, floor, and pow are others (modulo,
   negation, truncation, and exponentiation, respectively)
 - 213, 0, 123.20, 0.2 are valid numbers. (matching
   /[0-9]+(.[0-9]+)?/)

* Strings

like:
!#
"yes"
"\tab\newline"
"\\ backslash \" quote"
\word
#!
 - there are no keywords, string literals are just a quoted
   identifier and identifiers are just unquoted strings
 - a backslash \ quotes the next thing, but it's mostly just
   for identifiers when they are not meant to be immediately
   evaluated: !# (let \var val: body) #!
 - The strnth, strlen, strcat, strsub, and strrev functions
   index (0 based), measure, concatenate, slice, and reverse
   copies of their argument(s).

* Cons!

!#
conCurr # Lisp dotted-pairs
(: 1 2) # (1 . 2)
[: 1 2] # (nil . (1 . 2))
(1 2 3) # (((nil . 1) . 2) . 3)
{1 2 3} # (nil . (1 . (2 . (3 . nil))))
# nil is at ^ the beginning because braces {} and brackets []
# are automatically quoted
() # nil
[] # (nil . nil)
{} # (nil . nil)
\atom # (nil . atom)
#!
 - a pair of two values named l and r, car and cdr, or head
   and tail.
 - all structure is built out of these
 - unlike lisp, spaces in parentheses are right-associative,
   which is why conCurr has cons in its name instead of list
 - "proper" lists can be made with braces {}
 - lstnth, lstlen, lstcat, lstrev, map, reduce, assoc, l, r,
   setl! and setr! are standard list processing functions
 - cons to pair two things together

(assoc {a-list} key) example:
!#
(assoc {(: a 1) (: b 2) (: c 3)} \a) ==> (: a 1)
#!

* Functions

 - all functions are first class and have one argument
 - Church booleans are used instead of a boolean type.
   Here are their definitions:
!#
(define [true  a b]: value a)
(define [false a b]: value b)
#!
 - predicates can return their own specialized function for
   true, e.g. to bind values but always use false for false
 - let's write some boolean-processing functions!
!#
(define [and a b]
  : ... )

(define [or a b]
  : ... )

(define [not a]
  : ... )

(define [xor a b]
  : ... )

# and a challenge
(define [lstnth lst n] # 0-based, of course
  : ... )

#!
That's all folks, for now
!#