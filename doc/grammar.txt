# note: supportedpragma is by default nothing

ws <- [' \n\t\r'] | comment
comment <- '#!' (comment | char)* '!#'
         | '#' [^supportedpragma] [^'\n']* '\n'
         | '#\n'
         | '#;' expr

expr <- ws* val
val <- '\' expr
     | string
     | struct
     | list
     | atom  # numbers are just a special case of atoms
     | '#' [supportedpragma] [^'\n']* '\n'

string <- '"' ([^'"\\'] | '\\' char)* '"'

struct <- '(' body ')'
        | '$' body
        | '[' body ']'
        | ':' body

body <- (':' expr)? expr* ws?

list <- '{' expr* ws? '}' (':' expr)?
  # {a b}:c => (a b . c) in lisp

atom <- [^' \n\t#:{}()[]"$|']+
      | '|' ([^'|\\']* | '\\' char)* '|'


