
Concurr is getting a rewrite for these reasons:

- Make the syntax better
- Change the name (Concurr isn't very related to concurrency)
- Clean things up (the lua folder is pretty useless and stuff)
- I know more about languages and design, so things will be fixed

The core evaluation strategy will not change. The rewrite will still be

- Curried
- Purely functional
- Syntax similar to s-expressions
- Awesome
- RAWMEAT (Restrictions aren't worth my effort and time :)
- Vegan :)

---

 conCurr ![logo](cnc-bmp.gif)
============================

Inspired by lisp (scheme, robin, and picolisp), (oca)ml, [red](https://github.com/red/red), and logo. It also shares some features with [hoon](https://urbit.org/blog/why-hoon/).

You can use [paste](https://ermineii.github.io/paste) to paste syntax-highlighted code.

 Features
----------------------------

- Currying
- Homoiconicity -- that looks good
- Pure Functional Programming
- Free-form syntax
- Pattern matching
- Custom control structures
- Macros?

 Roadmap
----------------------------

- [x] Name, logo
- [x] Basic syntax
- [ ] Solidify syntax -- *probably finished*
- [x] Working lexer and parser
- [ ] Full specification
- [x] Basic tree-walk interpreter -- basics done, not all built-ins
- [ ] Get people interested -- *hard*
- [ ] Compile to scheme
- [ ] Create a bytecode VM, compile to it
- [ ] Self-host!
- [ ] Compile to C, JS, WASM ...
- [ ] Add some optimizations

 Syntax
----------------------------

You can now find a tutorial in the [tutorial.cnc](doc/tutorial.cnc) file.

