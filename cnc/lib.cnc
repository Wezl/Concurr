#!/bin/sh
./cnc "$0" -- "$@" || cnc "$0" -- "$@"; exit;

# this isn't meant to be a standalone program, so
# maybe this header should be removed.

### Standard library for conCurr
### (c) Cedrice 2021
### Licensed under the Artistic License v2.0
!#

#!
builtins (not in a library):
  functions:
    lambda lambda? true false
  thunks:
    safely do compile! return
  cons:
    left right cons cons? null? nil
  definition:
    define let proc defined? letrec
  numbers:
    mul add mod div sub number? neg less more floor
  strings:
    strnth ascii strlen strcat sym->str string?
  symbols:
    str->sym symbol?
  i/o:
    writestr! readline!
  other:
    equ link require error

library functions (guaranteed to be available, but they may be optimized):
  function:
    and not or fn
  definition:
    let*
  cons:
    cons^ cons^! list? map reducel scanl lstnth lstlen
  control:
    while case-with if foreach
  numbers:
    range notmore notless
  numbers, in Math library:
    sqrt cos sin ... 
  other:
    notequal
!#

#!# (cons^ \head \tail mbcons [ifso] [ifnot])
 ! 

#!# (match-any? {"c1" "c2"} "c")
 ! is c equal to any item in the list?
 !#
(define [match-any? is i]
   : cons^ \i' \is is
      [if (= i' i)
         \true
         : match-any? is i
      ]
    : false
)

#!# (remove-while (fn?) {lst})
 ! trim off any items that return true for fn? at the beginning
 !#
(define [remove-while p? lst]
   : null? lst \()
   : p? (left lst) [remove-while p? $right lst]
    \lst
)

(define [cons^! lname rname c body]
  : let lname (left c)
  : let rname (right c)
   body
)

(define [fn names body]
   : null? (right names)
      [lambda (left names) body]
    : 

#!# (range i) => {0 1 2 ... i - 1}
 ! (range i<=0) => {}
 !#
(define [range i]
  : let \iter (fn {accum i}
    : let \i (- i 1)
     : < i 0 accum
      : iter (cons i accum) i
  )
   : iter \() i
)

#!# (reducel (fn) start-value {lst})
 ! (reducel (+) 0 {0 5 3 12 1}) =>
 !# (+ (+ (+ (+ (+ 0 0) 5) 3) 12) 1)
(define [reducel func accum lst]
   : null? lst \accum
    : reducel func
              (func accum $left lst)
              $right lst)



