
### conCurr symbolic shortcuts for stdlib functions
### (c) Cedrice 2021
### Licensed under the Artistic License v2.0

(require "lib")

(define [nth n seq]
   : cons? seq [listnth n seq]
   : strnth n seq
)

#\opt: gen-defines
(case-with (lambda \name:
            lambda \body:
            lambda \next: do2 (define name body) next)
  {/ (lambda \a: number? a [div a] [fn a])
   + add
   * mul
   @ nth
   < less
   <= notmore
   = equ
   /= notequal
   >= notless
   > more
   ! not
   .. strcat
   ... range
   ; cons
   . left
   l left
   , right
   r right

   # unused: ~`!%^&|?;,.'
})
